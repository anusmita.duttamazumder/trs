############
Introduction
############
Developing software on its own is complicated and requires time, skills and lots
of efforts. But being good at writing individual software isn't sufficient in
this day and age. Systems are inherently complicated, with lots of components
interacting with each other. We have to deal with intracommunication as well as
external communication with remote systems. All aspects of security have to be
considered, standards need to be addressed and systems need to be tested not
only as individual components, but as coherent systems. For device
manufacturers, this becomes a real challenge, which is very costly both in terms
of time and effort.

As an answer to the challenges presented, Linaro has created **TRS (Trusted
Reference Stack)**, an umbrella project and software stack that includes well
tested software.  Its components make up a solid base for efficient development
and for building unique and differentiating end-to-end use cases.

Goals and key properties
************************
* Common platform for deliverables from Linaro.
* Include all Linaro test suites and test frameworks making CI/CD and regression
  testing fast, valuable and efficient.
* An efficient development environment for engineers.
* A product ready reference implementation.
* Configurable to be able to meet different needs.
* Common ground and building blocks for Blueprints and similar targets.
* Interoperability making it possible to use alternative implementations.
* Pre-silicon IP support in environments like QEMU etc.

High-level overview
*******************
The TRS software stack is made up of application software that spans all
architectural layers, from low-level programs that communicate directly with
hardware to the user-space environment. To make it a flexible solution and be
able to partition workloads, it also has support for running applications either
as containerized applications or as virtual machines. The image below shows a
high level overview of the key components in TRS.

.. image:: images/introduction-high-level-overview-01.png
  :alt: High level overview of the TRS architecture

Firmware software components
****************************
Firmware in TRS is provided by Trusted Substrate, which has its own dedicated
area in this documentation (see :ref:`Firmware - Trusted Substrate`).

Use-cases and features overview
*******************************
- Deployment of application workloads via Docker, Podman and k3s.
- Orchestration of resource-managed and isolated application workloads via
  Docker, K3s, and the Xen type-1 hypervisor providing hardware virtualization.
- Remote updates for all software components, to be able to address security
  issues, fix bugs and add features. Includes optional A/B updates.
- Measured boot to facilitating sealed keys technologies.
- Encrypted filesystem to meet confidentiality needs.
- Remote attestation to be able to trust and be trusted by remote parties.
- Using backend devices via VirtIO interfaces.
- Early development of new architectural features.
- Ability to load different OSes and distros without hard coded dependencies to
  firmware (EBBR, SystemReady).
- General Arm ecosystem enablement.
- Regression testing on merge requests and on daily builds.

TRS system architectures
************************
TRS is compatible with both bare-metal and virtualized environments. This allows
TRS to be utilized for a variety of application cases. When needed on typical
embedded devices, baremetal is likely the best option; but, if you need to run
multiple payloads, operating systems, etc., virtualization architecture may be a
better fit. In the section :ref:`System Architectures`, you will find a detailed
overview of the two different architectures.

Feedback and support
********************
To request support please contact Linaro at support@linaro.org.

Maintainer(s)
*************

- Joakim Bech <joakim.bech@linaro.org>
- Ilias Apalodimas <ilias.apalodimas@linaro.org>
- Mikko Rapeli <mikko.rapeli@linaro.org>
