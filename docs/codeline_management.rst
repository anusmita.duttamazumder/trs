..
 # Copyright (c) 202, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

###################
Codeline Management
###################
TRS is a software development project that uses the release branch approach of
Yocto. We intend to base our work and use the latest version available from the
Yocto project. As mentioned before, the goal of the project is to create a
reliable, stable builds by leveraging stable upstream branches and thereby
reducing the risk of build and runtime issues.

Release process
***************
By basing each release on a stable branches, the TRS project ensures
reproducibility and that releases should work for a foreseeable future. The
approach for making a release is as follows.

**Create a manifest with stable commits**: When a release is approaching, a
stable snapshot is created by writing all current commits to a temporary repo
xml file. This is achieved by running:

 .. code:: bash

   $ repo manifest -r -o my-release.xml

After this step we have well defined commits that won't change as long as we
don't re-run or edit the file. This serves as the basis for our release
testing.

**Sanity testing**: With the tagged manifests we start doing sanity testing.

**Tag our own gits**: After successful testing, we will tag the gits that we
control and own. The tag in question is the version in the form
``vMAJOR.MINOR`` (from the `Semantic Versioning`_).

**Create a release branch**: Next, we create a release branch for TRS in the
``trs-manifest.git`` repository, which will have the same name as the tag. This
is important, because we use the ``v<Major>.<Minor>`` reference differently when
checking out the release branch via repo. I.e., even though the names are the
same, they have distinct uses.

**Replace commits with tags**: With the tags also added and the release branch
created, we edit and update the ``default.xml`` file with the commits we got
when creating the stable commits and with the tags that we've created for the
gits that we own.

**Update the release page**: We also want to document the release. So, we need
to add a section about our release at the :ref:`Changelog & Release Notes` page.

**Push tags and release branch**: The final step of making a release is to (git)
push the tags and the branches to the upstream tree's. Once done, the release
has been completed.

Release cadence
***************
Until now releases has been a bit ad-hoc, but we plan to move to quarterly
release cadence and we'll try to align them with the release of Trusted
Substrate.

Branches
********
We provide a :ref:`Developer setup` for TRS and we also provide :ref:`Release
build` for TRS. Conceptually, the only difference between these is the tagging
and branching strategy. A developer build is what you get when checkout out the
repo manifest without providing any branch, i.e. ``-b`` is not used. The
developer build typically follows tip at upstream for the gits that we own and
control. Other gits that we use tends to be locked to a certain tag or a
commit. This is an intentional trade-off where we will have the ability to run
latest on our own gits, but still not be affected by other issues that perhaps
shows up in other gits.

In contrast, the release branches contain either commits or tags for all gits in
their respective manifest files. Therefore, stable branches never follow the tip
(which would make the non-stable).

So in summary, the ``main`` branch in TRS is for developers wanting to run the
latest available and the branches named ``v<Major>.<Minor>`` are the stable
branches.

.. _Semantic Versioning: https://semver.org/
