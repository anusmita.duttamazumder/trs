#
# SPDX-License-Identifier: MIT
#

from subprocess import Popen, PIPE
import time

from oeqa.runtime.case import OERuntimeTestCase
from oeqa.core.decorator.depends import OETestDepends
from oeqa.core.decorator.oetimeout import OETimeout
from oeqa.core.decorator.data import skipIfQemu

class TRSXenTest(OERuntimeTestCase):

    @OETimeout(600)
    @OETestDepends(['ssh.SSHTest.test_ssh'])
    def test_xen(self):
        # Set default GRUB entry as 1 for Xen booting
        cmd = 'sed -i "s/set default=\"0\"/set default=\"1\"/" /boot/EFI/BOOT/grub.cfg'
        status, output = self.target.run(cmd)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))

        cmd = 'reboot'
        status, output = self.target.run(cmd)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))

        # enough time for a reboot into already encrypted rootfs so
        # this should be fast
        time.sleep(200)

        cmd = 'xl list'
        status, output = self.target.run(cmd)
        self.assertRegex(output, "Domain-0", msg='\n'.join([cmd, output]))

        # Rostore the default GRUB entry to 0
        cmd = 'sed -i "s/set default=\"1\"/set default=\"0\"/" /boot/EFI/BOOT/grub.cfg'
        status, output = self.target.run(cmd)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))

        cmd = 'reboot'
        status, output = self.target.run(cmd)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))

        # enough time for a reboot into already encrypted rootfs so
        # this should be fast
        time.sleep(200)

        # System runs in bare-metal mode, so xl list reports error.
        cmd = 'xl list'
        status, output = self.target.run(cmd)
        self.assertRegex(output, "cannot init xl context", msg='\n'.join([cmd, output]))
