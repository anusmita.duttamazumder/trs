#
# SPDX-License-Identifier: MIT
#

import os

from oeqa.runtime.case import OERuntimeTestCase
from oeqa.runtime.decorator.package import OEHasPackage
from oeqa.core.decorator.oetimeout import OETimeout

class TpmTestSuite(OERuntimeTestCase):
    """
    Run TPM test cases.
    """
    @OETimeout(120)
    @OEHasPackage(['tpm2-tools'])
    def test_tpm(self):
        cmd = "tpm2_selftest -f ; tpm2_gettestresult"
        status, output = self.target.run(cmd, 60)
        self.assertRegex(output, "status:   success", msg='\n'.join([cmd, output]))
