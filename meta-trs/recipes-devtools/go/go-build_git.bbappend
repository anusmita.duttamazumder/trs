# Patch has been accepted, but it's only in master-next
# https://lists.yoctoproject.org/g/meta-virtualization/message/8250
SRCREV_FORMAT = "runx_runc"
