# remove dependencies which bring too complex things to the rootfs
# without really good reasons
RDEPENDS:remove = "git"
RDEPENDS:remove = "openssl-bin"
RDEPENDS:remove = "perl"
RDEPENDS:remove = "python3-pyyaml"
RDEPENDS:remove = "vim"
